# Lecture 5 Performance Measures

Performance measure is an important topic in traffic operation. With big data in transportation, better performance measure can be proposed and used to discover more insights in traffic operation from big picture. This lecture introduces the performance measure systems developed by REACTOR lab with big data enabled.