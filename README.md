# CE 650C - Advanced Topics in Transportation Engineering: Data Analysis

## Introduction

Increased monitoring of transportation network via traditional and non-traditional techniques will only be fruitful if a timely analysis can be conducted for accurate assessment of traffic conditions. The rate of collection of such data is poised to increase, with more closed circuit cameras, community based sensing such as Waze, instrumentation of taxis and bus systems, and vehicle to infrastructure communications. Rigorous mining of such data can lead to several insights that might be buried in enormous data logs.

Recent advances in big data software (eg. Hadoop 2, SPARK), distributed file system (eg. HDFS, RDD), and databases (HBASE, HIVE) have made it feasible to operate on big quantities of data, coming in real time from heterogeneous data sources. These systems operate on a cluster of commercially available servers instead of high end machines. The advantage of such a system is that high performance capability can be achieved at a very reasonable cost.

In recent years, there has been a big push in developing big data analytics in the areas of computer science and business analytics. Despite being the perfect use case for applying big data analytics, the trend has not caught up in the area of transportation.

You will find that this class is a window to the world of big data analytics specifically focused to the the domain of transportation engineering. You will be exposed to lot these technologies through hands on activities over the course of this course!

## What You Will Do

You will have three primary tasks during this course:

* Build a knowledge base on why do we need big data analytics in transportation.
* Use big data tool for simple performance analytics.
* Use big data tools for advanced machine learning applications.

You will be using an actual big data cluster environment in which you will learn by observing, coding and experimenting. 

## Lectures

Each lecture has a specific topic and corresponding materials including presentation, sample codes and homework assignment. You can find the materials in `source` folder.

