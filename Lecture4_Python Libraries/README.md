# Lecture 4 Python Libraries: Numpy and Matplotlib

This lecture introduces the usage of several Python libraries regarding data processing and visualization. Numpy is the most commonly used data processing library and matplotlib is used for data visualization with similar features in matlab software. The materials in this lecture include:

* Presentation on lecture
* Hand-on codes in using those libraries
* Assigment on data processing and visualization
* Assigment input data