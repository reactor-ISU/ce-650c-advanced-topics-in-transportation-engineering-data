# Lecture 3

This lecture introduces Python language and demonstrates on how to download real-time data feed by using Python program. The materials include:

* Presentation on lecture
* Hand-on codes in Python to download data
* Assignment

## Notice

The real-time feed can be accessed by this URL:

http://205.221.97.102/Iowa.Sims.AllSites.C2C/IADOT_SIMS_AllSites_C2C.asmx/OP_ShareTrafficDetectorData?MSG_TrafficDetectorDataRequest=string%20HTTP/1.1
