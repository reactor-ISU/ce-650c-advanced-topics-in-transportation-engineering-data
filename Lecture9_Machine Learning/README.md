# Lecture 9 Machine Learning

This lecture introduces several machine learning algorithms, mainly focusing on classification and clustering. The materials include:

* Presentation on classification methodology and code example
* Hand-on code in classifier playground 
* Presentation on clustering algorithm
* Assignment regarding data smoothing, classification and prediction