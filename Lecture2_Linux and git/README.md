# Lecture 2 Linux and git

This lecture covers some basic commands in Linux system, basic knowledge in git and codes illustrating how to use git.

The materials include:

* Presentation on lecture
* Hand-on codes and exercises in git operation
